package com.example.toast

import android.content.Context
import android.widget.Toast




object Utils {

    fun printToast(c: Context?, message: String?) {
        Toast.makeText(c, message, Toast.LENGTH_SHORT).show()
    }

}


